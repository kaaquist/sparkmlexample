.PHONY: all jar

build: 
	sbt clean compile

clean: 
	sbt clean

test: 
	sbt clean test

jar:
	sbt assembly
