# Apache ML example  

Trying to figure out how to use Apache Spark with AWS sagemaker.

First off I have taken an example from here: [link](https://www.bmc.com/blogs/k-means-clustering-apache-spark/)

Next is to set it up with AWS sagemaker following this here guide: [link](https://docs.aws.amazon.com/sagemaker/latest/dg/apache-spark.html)
