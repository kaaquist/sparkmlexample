name := "mlexample"
organization := "com.funny"
version := "0.0.1"

scalaVersion := "2.12.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-feature", "-encoding", "utf8")

mainClass in assembly := Some("com.funny.Main")

artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.withClassifier(Some("assembly"))
}
addArtifact(artifact in (Compile, assembly), assembly)

libraryDependencies ++= {
  val apacheSparkVersion = "2.4.3"
  Seq (
    //Logging and Configuration
    "com.typesafe.scala-logging"            %% "scala-logging"                % "3.9.2",
    "ch.qos.logback"                         % "logback-classic"              % "1.2.3",
    "com.github.pureconfig"                 %% "pureconfig"                   % "0.11.1",

    // Spark
    "org.apache.spark"                %% "spark-core"           % apacheSparkVersion,
    "org.apache.spark"                %% "spark-mllib"          % apacheSparkVersion,
    "org.apache.spark"                %% "spark-sql"            % apacheSparkVersion,
    "org.apache.spark"                %% "spark-streaming"      % apacheSparkVersion,
  )
}
