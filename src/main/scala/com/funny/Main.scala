package com.funny

import com.funny.util.Config
import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.SparkConf
import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.sql.SparkSession
import org.apache.log4j.{Level, Logger}

object Main extends App with LazyLogging {
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  val conf = new Config()
  System.setProperty("HADOOP_HOME", conf.hadoopConfig.binDir)
  val sparkConf = new SparkConf().setAppName("Application").setMaster("local[2]")
  sparkConf.set("spark.driver.allowMultipleContexts","true")
  val ss = SparkSession.builder.config(sparkConf).getOrCreate()
  val ctx = ss.sparkContext
  ctx.setLogLevel("ERROR")

  var rdd = ctx.textFile(getClass.getResource("/crime_data.csv").getPath)

  import ss.implicits._

  def stoDouble (s : String): Double = {
    s.map(_.toByte.doubleValue()).reduceLeft( (x,y) => x + y)
  }

  case class StateCode(State:String, Code:Double)
  val header = rdd.first()
  val lines = rdd.filter(row => row != header).map(l => l.split(","))
  val states = lines.map(l => StateCode(l(0),stoDouble(l(0)))).toDF()
  states.show()
  states.createOrReplaceTempView("states")

  def makeDouble (s: String): Array[Double] = {
    val str = s.split(",")
    val a = stoDouble(str(0))
    Array(a,str(2).toDouble,str(3).toDouble,str(4).
      toDouble,str(5).toDouble)
  }

  val crime = rdd.filter(row => row != header).map(m => makeDouble(m))

  val crimeVector = crime.map(a => Vectors.dense(a(0),a(1),a(2),a(3),a(4)))
  val clusters = KMeans.train(crimeVector,5,10)

  case class Crime (Code:Double, Murder:Double, Assault:Double, UrbanPop:Double,Rape:Double, PredictionVector:org.apache.spark.mllib.linalg.Vector, Prediction:Double)
  val crimeClass = crimeVector.map(a => Crime(a(0), a(1), a(2), a(3), a(4), a ,clusters.predict(a))).toDF()
  crimeClass.show()
  crimeClass.createOrReplaceTempView("crimes")
  ctx.stop()
}
