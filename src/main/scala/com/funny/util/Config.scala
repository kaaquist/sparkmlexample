package com.funny.util

import com.typesafe.scalalogging.LazyLogging
import pureconfig.generic.ProductHint
import pureconfig.{CamelCase, ConfigFieldMapping, SnakeCase}
import pureconfig.generic.auto._

case class Config() extends LazyLogging {
  import Config._
  implicit def hint[T] = ProductHint[T](ConfigFieldMapping(CamelCase, SnakeCase))
  lazy val prefixRoot = "funny"
  lazy val hadoopConfig: Hadoop = pureconfig.loadConfigOrThrow[Hadoop](s"$prefixRoot.hadoop")
}

object Config {
  val theConf: Config = Config()

  case class Hadoop(
                    binDir: String
                        )
}